from collections import deque
import math
import random

FREE = 0 # stored in matrix
WALL = 1 # stored in matrix
WORM = 2 # stored in matrix and body
FOOD = 3 # stored in list of Item

items = None
worms = None
field = None

def get_randxy(size):
    return (random.randint(0, size[0] - 1), random.randint(0, size[1] - 1))

# x from left to right
# y from top to bottom
class Element:
    angles = (0, math.pi * 2 / 3, math.pi * 2 / 3 * 2)
    # Switch to opposite
    oswitcher = {
        'r': 'l',
        'l': 'r',
        'u': 'd',
        'd': 'u'
    }
    dxswitcher = {
        'r': +1,
        'l': -1
    }
    dyswitcher = {
        'd': +1,
        'u': -1
    }
    # Switch to relative coords
    cswitcher = {
        'r': (+1, 0),
        'l': (-1, 0),
        'd': (0, +1),
        'u': (0, -1)
    }
    # Switch to relative coords of tile in direction of tail
    cswitcher_of_tail = {
        'r': (-1, 0),
        'l': (+1, 0),
        'd': (0, -1),
        'u': (0, +1)
    }
    def __init__(self, d_head, d_tail, xy):
        self.offset_angle_tail = random.random() * math.pi * 2
        self.offset_speed_tail = random.random() / 5 + 0.1
        self.angles = self.projections = None
        self.update_angles()
        self.d_head = d_head # direction to head
        self.d_tail = d_tail # direction to tail
        self.xy = xy
    @property
    def d_head(self):
            return self.__d_head
    @property
    def d_tail(self):
            return self.__d_tail
    @d_head.setter
    def d_head(self, d_head):
        self.__d_head = d_head
        self.c_head = Element.cswitcher.get(d_head) # normalized coordinates of tile showing to head
    @d_tail.setter
    def d_tail(self, d_tail):
        self.__d_tail = d_tail
        self.c_tail = Element.cswitcher.get(d_tail) # normalized coordinates of tile showing to tail
        self.c_head_of_tail = Element.cswitcher_of_tail.get(d_tail) # normalized coordinates of tile in direction of tail showing to head
    #def __eq__(self, other):
    #    return self.d == other.d
    def __str__(self):
        return "{}({:02d},{:02d})".format(self.d, self.xy[0], self.xy[1])
    @staticmethod
    def opposite_direction(d):
        return Element.oswitcher.get(d)
    @staticmethod
    def dx(d):
        return Element.dxswitcher.get(d, 0)
    @staticmethod
    def dy(d):
        return Element.dyswitcher.get(d, 0)
    def update (self, step, step_max):
        self.offset_angle_tail += (self.offset_speed_tail / step_max)
        self.update_angles()
    def update_angles(self):
        self.angles = [a + self.offset_angle_tail for a in Element.angles]
        self.projections = [math.sin(a) * 0.7 for a in self.angles]


class Worm:
    def __init__(self, element):
        self.completion = 1.0
        self.had_accident = False
        self.points = 0
        self.newdirection = element.d_head
        self.body = deque()
        self.body.append(element)
        self.lost_tail = None
    def __str__(self):
        mybody = ''
        for mye in list(self.body):
            mybody += str(mye) + ' '
        return mybody
    def changedir(self, newdirection):
        if newdirection != Element.opposite_direction(self.body[0].d_head):
            self.newdirection = newdirection
    # Move and feed only, no collision detection
    def move(self, auto_grow = False):
        had_food = False
        self.lost_tail = None
        self.had_accident = False
        self.body[0].d_head = self.newdirection
        e_head = self.body[0]
        xy = ((e_head.xy[0] + Element.dx(self.newdirection)) % field.size[0],
             (e_head.xy[1] + Element.dy(self.newdirection)) % field.size[1])
        e = Element(self.newdirection, Element.opposite_direction(self.newdirection), xy)
        # check for obstacles
        if field.get(e.xy) != FREE:
            self.had_accident = True
            return
        self.body.appendleft(e)
        field.set(e.xy, WORM)
        # check for food
        for i in items:
            if e.xy == i.xy:
                if i.it == FOOD:
                    had_food = True
                    self.points += 1
                    i.consumed()
        if (had_food == False) and (auto_grow == False):
            field.set(self.body[-1].xy, FREE)
            self.lost_tail = self.body.pop()
    def update_body(self, step, step_max):
        self.completion = (step / step_max)
        for e in self.body:
            e.update(step, step_max)
        if self.lost_tail is not None:
            self.lost_tail.update(step, step_max)

class Field:
    def __init__(self, size):
        width, height = self.size = size
        self.matrix = [[0 for _ in range(height)] for _ in range(width)]
    def set(self, xy, what):
        self.matrix[xy[0]][xy[1]] = what
    def get(self, xy):
        return self.matrix[xy[0]][xy[1]]
    def update_items(self):
        for i in items:
            i.update()
    def get_randitemxy(self):
        xy = None
        while xy is None:
            xy = get_randxy(self.size)
            if self.get(xy) != 0:
                xy = None
            else:
                for i in items:
                    if xy == i.xy:
                        xy = None
                        break
        return xy

class Item:
    def __init__(self, item_type):
        self.it = item_type
        self.xy = None
        self.tt = None # total time of cycle
        self.ttl = None # time to live
    #def is_item(self, item_type):
    #    return self.it == item_type
    def update(self):
        if (self.tt is None) or (self.tt < 0):
            self.tt = random.randint(50, 80)
            self.ttl = random.randint(40, 90)
            self.xy = None
        elif self.tt == self.ttl:
            self.xy = field.get_randitemxy()
        self.tt -= 1
    def consumed(self):
        self.tt = random.randint(100, 200)
        self.ttl = random.randint(40, 90)
        self.xy = None
    def is_active(self):
        return self.xy is not None

