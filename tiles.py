from data import Element
import sys
import pygame
import math
from collections import deque


def tupadd(tup1, tup2):
    return tuple(map(lambda x, y: x + y, tup1, tup2))


pygame.init()


# Percentage, start, first coord, second coord, end
def getBez(percent, p1, cp1, cp2, p2):
    def b1(t): return t*t*t
    def b2(t): return 3*t*t*(1-t)
    def b3(t): return 3*t*(1-t)*(1-t)
    def b4(t): return (1-t)*(1-t)*(1-t)
    return (p1[0]*b1(percent) + cp1[0]*b2(percent) + cp2[0]*b3(percent) + p2[0]*b4(percent),
           p1[1]*b1(percent) + cp1[1]*b2(percent) + cp2[1]*b3(percent) + p2[1]*b4(percent))

# Percentage, start coord, end coord
def getLin(percent, xy1, xy2):
    p1 = percent
    p2 = 1.0 - percent
    return xy1[0] * p2 + xy2[0] * p1, xy1[1] * p2 + xy2[1] * p1

WHITE = pygame.Color(255, 255, 255)
C = [[pygame.Color(255, 80, 80), pygame.Color(255, 0, 0), pygame.Color(120, 0, 0)],
    [pygame.Color(120, 255, 120), pygame.Color(0, 180, 0), pygame.Color(0, 120, 0)]]
BLUE = pygame.Color(90, 90, 255)
YELLOW = pygame.Color(200, 200, 0)
BLACK = pygame.Color(0, 0, 0)

def some_tile(tilesize):
    rect_border = pygame.Surface(tilesize)  # Surface to draw on.
    rect_border.set_alpha(20)
    pygame.draw.rect(rect_border, WHITE, (1, 1, tilesize[0] - 2, tilesize[1] - 2))
    #rect_border.fill(WHITE)
    return rect_border

def food_tile(tilesize):
    center = tilesize[0] // 2, tilesize[1] // 2
    rect_border = pygame.Surface(tilesize)  # Surface to draw on.
    #rect_border.fill(YELLOW)
    pygame.draw.circle(rect_border, BLUE, center, 8)  # Draw on it.
    return rect_border

def getPointString(points, color_scheme, font):
    points_string = "{:04d}".format(points)
    text = font.render(points_string, True, C[color_scheme][1])
    return text

class WormTile:

        #pygame.draw.circle(self.rect_border, WHITE, WormTile.denorm2D(currElement.c_head, tilesize), 10)
        #pygame.draw.circle(self.rect_border, WHITE, WormTile.denorm2D(currElement.c_tail, tilesize), 5)

    def __init__ (self, xy, tilesize):
        self.xy = xy
        self.rect_border = pygame.Surface(tilesize)  # Surface to draw on.
        self.tilesize = tilesize
        self.rect_border.set_colorkey(BLACK)
        #self.rect_border.set_alpha(255)

    def draw_line_norm(self, coords_head_norm, coords_tail_norm, i, color_scheme):
        pygame.draw.line(self.rect_border, C[color_scheme][i], WormTile.denorm2D(coords_head_norm, self.tilesize),
                                                               WormTile.denorm2D(coords_tail_norm, self.tilesize), 4)

    def draw_circle_norm(self, coords_norm, color_scheme, width):
        pygame.draw.circle(self.rect_border, C[color_scheme][1], WormTile.denorm2D(coords_norm, self.tilesize), width)

    @classmethod
    def head(cls, tilesize, e, completion, color_scheme):
        my_cls = cls(e.xy, tilesize)
        if completion >= 0.5:
            completion = (completion - 0.5) * 2.0 # first part of head movement is only on neck field
            h = getLin(completion, e.c_tail, (0.0, 0.0))
            for i in range(len(Element.angles)):
                if (e.d_tail == 'r') or (e.d_tail == 'l'):
                    t = e.c_tail[0], e.projections[i] * completion
                else:
                    t = e.projections[i] * completion, e.c_tail[1]
                my_cls.draw_line_norm(h, t, i, color_scheme)
            my_cls.draw_circle_norm(h, color_scheme, 8)
        return my_cls

    @classmethod
    def neck(cls, tilesize, eo, e, completion, color_scheme):
        my_cls = cls(e.xy, tilesize)
        if completion <= 0.5: # first part of movement: move center to border
            completion = completion * 2.0
            h = getLin(completion, (0.0, 0.0), e.c_head)
            for i in range(len(Element.angles)):
                #if (eo.d_tail == 'r') or (eo.d_tail == 'l'):
                #    h = eo.c_head_of_tail[0], eo.projections[i]
                #else:
                #    h = eo.projections[i], eo.c_head_of_tail[1]
                if (e.d_tail == 'r') or (e.d_tail == 'l'):
                    t = e.c_tail[0], e.projections[i]
                else:
                    t = e.projections[i], e.c_tail[1]
                my_cls.draw_line_norm(h, t, i, color_scheme)
            my_cls.draw_circle_norm(h, color_scheme, 8)
        else: # second part of movement: expand singular point on border to three points
            completion = (completion - 0.5) * 2.0
            for i in range(len(Element.angles)):
                if (eo.d_tail == 'r') or (eo.d_tail == 'l'):
                    h = eo.c_head_of_tail[0], eo.projections[i] * completion
                else:
                    h = eo.projections[i] * completion, eo.c_head_of_tail[1]
                if (e.d_tail == 'r') or (e.d_tail == 'l'):
                    t = e.c_tail[0], e.projections[i]
                else:
                    t = e.projections[i], e.c_tail[1]
                my_cls.draw_line_norm(h, t, i, color_scheme)
        return my_cls

    @classmethod
    def body(cls, tilesize, eo, e, color_scheme):
        my_cls = cls(e.xy, tilesize)
        for i in range(len(Element.angles)):
            if (eo.d_tail == 'r') or (eo.d_tail == 'l'):
                h = eo.c_head_of_tail[0], eo.projections[i]
            else:
                h = eo.projections[i], eo.c_head_of_tail[1]
            if (e.d_tail == 'r') or (e.d_tail == 'l'):
                t = e.c_tail[0], e.projections[i]
            else:
                t = e.projections[i], e.c_tail[1]
            my_cls.draw_line_norm(h, t, i, color_scheme)
        return my_cls

    @classmethod
    def tail(cls, tilesize, eo, e, completion, color_scheme):
        my_cls = cls(e.xy, tilesize)
        for i in range(len(Element.angles)):
            if (eo.d_tail == 'r') or (eo.d_tail == 'l'):
                h = eo.c_head_of_tail[0], eo.projections[i]
            else:
                h = eo.projections[i],  eo.c_head_of_tail[1]
            if (e.d_tail == 'r') or (e.d_tail == 'l'):
                t = e.c_tail[0], e.projections[i] * (1.0 - completion)
            else:
                t = e.projections[i] * (1.0 - completion), e.c_tail[1]
            my_cls.draw_line_norm(h, t, i, color_scheme)
        return my_cls

    @classmethod
    def lost_tail(cls, tilesize, eo, e, completion, color_scheme):
        my_cls = cls(e.xy, tilesize)
        for i in range(len(Element.angles)):
            if (eo.d_tail == 'r') or (eo.d_tail == 'l'):
                h = eo.c_head_of_tail[0], eo.projections[i] * (1.0 - completion)
            else:
                h = eo.projections[i] * (1.0 - completion),  eo.c_head_of_tail[1]
            t = getBez(completion, e.c_head, (0.0, 0.0), (0.0, 0.0), e.c_tail)
            my_cls.draw_line_norm(h, t, i, color_scheme)
        return my_cls


    #@staticmethod
    #def denorm(coord_rel, tilesize, direction):
    #    if direction == 'r':   return (+tilesize[0], coord_rel * tilesize[1] // 4 + tilesize[1] // 2)
    #    elif direction == 'l': return (0,            coord_rel * tilesize[1] // 4 + tilesize[1] // 2)
    #    elif direction == 'u': return (coord_rel * tilesize[0] // 4 + tilesize[0] // 2, 0)
    #    elif direction == 'd': return (coord_rel * tilesize[0] // 4 + tilesize[0] // 2, +tilesize[1])

    @staticmethod
    def denorm2D(xy_rel, tilesize):
        return (int(xy_rel[0] * tilesize[0] / 2 + tilesize[0] / 2), int(xy_rel[1] * tilesize[1] / 2 + tilesize[1] / 2))


class WormTiles:
    def __init__ (self, worm, tilesize, color_scheme = 0):
        self.tiles = list()
        eoo = eo = None
        neck = 0
        for e in worm.body:
            if eo is not None:
                if eoo is None:
                    self.tiles.append(WormTile.head(tilesize, eo, (1.0 if worm.had_accident else worm.completion), color_scheme))
                else:
                    if neck == 0:
                        self.tiles.append(WormTile.neck(tilesize, eoo, eo, (1.0 if worm.had_accident else worm.completion), color_scheme))
                        neck = 1
                    else:
                        self.tiles.append(WormTile.body(tilesize, eoo, eo, color_scheme))
            eoo = eo
            eo = e
        if worm.lost_tail is not None:
            self.tiles.append(WormTile.tail(tilesize, eoo, eo, worm.completion, color_scheme))
            self.tiles.append(WormTile.lost_tail(tilesize, eo, worm.lost_tail, worm.completion, color_scheme))
        else:
            self.tiles.append(WormTile.tail(tilesize, eoo, eo, 1.0, color_scheme))
