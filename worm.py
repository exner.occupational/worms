import data
import tiles
import sys
import pygame
import time
import random

pygame.init()
pygame.font.init()
pygame.joystick.init()
#font = pygame.font.SysFont("comicsansms", 72)
font = pygame.font.SysFont("monospace", 96, bold=True)
pygame.key.set_repeat(0, 0)


def tupmul(tup1, tup2):
    return tuple(map(lambda x, y: x * y, tup1, tup2))


tile_size = tw, th = 41, 41 # size of tiles
#tile_size = tw, th = 101, 101 # size of tiles
min_border = 10 # minimum of 10 pixel for border
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
sw, sh = pygame.display.get_surface().get_size() # size of display
iw = sw // 7 # width of info box

columns = (sw - iw - 3 * min_border) // tw # subtract info box width
rows = (sh - 2 * min_border) // th
board_size = columns, rows
ih = rows * th
bw, bh = (sw - iw - columns * tw) // 3, (sh - rows * th) // 2 # border sizing
iow, ioh = 2 * bw + columns * tw, bh # offset of info box

KL = [pygame.K_LEFT, pygame.K_a]
KR = [pygame.K_RIGHT, pygame.K_d]
KU = [pygame.K_UP, pygame.K_w]
KD = [pygame.K_DOWN, pygame.K_s]

for i in range(pygame.joystick.get_count()):
    pygame.joystick.Joystick(i).init()

# screen = pygame.display.set_mode(screensize)
clock = pygame.time.Clock()

data.items = [data.Item(data.FOOD) for _ in range(48)]
#data.worms = [data.Worm(data.Element('l', 'r', (2, 2)))]
data.worms = [data.Worm(data.Element('r', 'l', (12, 4))), data.Worm(data.Element('l', 'r', (5, 5)))]
data.field = data.Field(board_size)

fps = 40
fps = fps * len(data.worms)
step = step_max = 5 # <step_max> Graphic updates per one model update

for w in data.worms:
    for i in range(3): w.move(True)

def main():
    while 1:
        for w in data.worms:
            #step = step % step_max + 1
            for step in range(1, step_max + 1):
                # for event in pygame.event.get():
                # Workaround for buggy handling of key events (on Linux?)
                #el = list()
                #for _ in range(2):
                #    time.sleep(0.005)
                #    el += pygame.event.get()
                el = pygame.event.get()
                for event in el:
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_q:
                            pygame.quit()
                            sys.exit()
                        c = 0
                        for ww in data.worms:
                            if event.key == KL[c]:
                                ww.changedir('l')
                            elif event.key == KR[c]:
                                ww.changedir('r')
                            elif event.key == KU[c]:
                                ww.changedir('u')
                            elif event.key == KD[c]:
                                ww.changedir('d')
                            c += 1
                    elif event.type == pygame.JOYBUTTONDOWN:
                        c = 0
                        for ww in data.worms:
                            if (event.joy == c) and (event.button == 0):
                                ww.changedir('l')
                            if (event.joy == c) and (event.button == 3):
                                ww.changedir('r')
                            if (event.joy == c) and (event.button == 2):
                                ww.changedir('u')
                            if (event.joy == c) and (event.button == 1):
                                ww.changedir('d')
                            c += 1

                #screen.fill((0, 0, 0))
                screen.fill((200, 200, 200))
                pygame.draw.rect(screen, (0, 0, 0), (bw, bh, columns * tw, rows * th))
                pygame.draw.rect(screen, (100, 0, 0), (iow, ioh, iw, ih))

                if step == 1:
                    data.field.update_items()
                    w.move()
                w.update_body(step, step_max)

                # Check Accidents
                # if data.field.get(w.body[0].xy) != 0:
                #    print('Accident')

                # Print Item
                for i in data.items:
                    if i.is_active():
                        screen.blit(tiles.food_tile(tile_size), (bw + i.xy[0] * tw, bh + i.xy[1] * th))

                # Print Worm & Points
                c = 0
                for ww in data.worms:
                    wts = tiles.WormTiles(ww, tile_size, c)
                    for wt in wts.tiles:
                        screen.blit(wt.rect_border, (bw + wt.xy[0] % columns * tw, bh + wt.xy[1] % rows * th))
                    pts = tiles.getPointString(ww.points, c, font)
                    screen.blit(pts, (iow + 20, ioh + 20 + c * 200))
                    c += 1

                # Print Field
                #for x in range(columns):
                #    for y in range(rows):
                #        if data.field.get((x, y)) != data.FREE:
                #            screen.blit(tiles.some_tile(tile_size), (bw + x * tw, bh + y * th))

                pygame.display.flip()
                # pygame.display.update()
                print(clock.get_fps())
                clock.tick(fps)

if __name__ == '__main__':
    import cProfile as profile
    profile.run('main()')
    #main()
